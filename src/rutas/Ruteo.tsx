import { Route, Routes } from "react-router-dom";
import { Acceso } from "../componentes/Acceso";

export const Ruteo = () => {
  return (
    <Routes>
      <Route path="/entrar" element={<Acceso />} />
    </Routes>
  );
};
