import React from "react";
import "./App.css";
import { Cabecera } from "./componentes/Cabecera";
import { BrowserRouter } from "react-router-dom";
import { Ruteo } from "./rutas/Ruteo";

function App() {
  return (
    <div className="container-fluid">
      <BrowserRouter>
        <Cabecera />
        <Ruteo />
      </BrowserRouter>
    </div>
  );
}

export default App;
